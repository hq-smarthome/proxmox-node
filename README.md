# Proxmox Node

Instructions and scripts for building and deploying a proxmox node within hQ.

This is not the most ideal set-up as I am limited by the hardware I have available to me currently.

## Cluster

I currently run a single node with plans on building a cluster in the future with 3 physical nodes. For now the hardware is:

> **Node A**
>
> Re-purposed Dell Inspiron 580
> * CPU: Intel i5 2nd Gen
> * RAM: 6GB DDR3
> * NIC: 2x Gigabit (1x on-board + 1x Intel PCI-E )
> * SSD: RAID 1 ZFS (2x 64GB SanDisk SSD)

> **Node B (Future Plan)**
>
> Synology DS1618+
> * CPU: Intel Atom C3538
> * RAM: 4GB DDR4 (Plan to upgrade)
> * NIC: 4x Intel Gigabit

> **Node C (Future Plan)**
>
> Undecided

## Prerequisites

These instructions assume that the network is already configured and that there is a synology DS1618+ already configured on the network.

## Table of Contents

- [Proxmox OS](#proxmox-os)

## Instructions

### Proxmox OS

Currently Node A is set up using a RAID 1 ZFS implemented on two 64GB SanDisk SSDs

For installation instructions for Proxmox please refer to their [Installation wiki page](https://pve.proxmox.com/wiki/Installation)
